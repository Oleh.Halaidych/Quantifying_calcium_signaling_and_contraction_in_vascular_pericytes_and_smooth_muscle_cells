----------------------------------------------------------------------------------------
Quantifying Ca2+ signaling and contraction in vascular pericytes and smooth muscle cells 
----------------------------------------------------------------------------------------

Here we provide R-scripts and CellProfiler pipelines for automated image and data analysis 
of Ca2+ release and contraction of pericytes and smooth muscle contraction. 
(Image analysis of Ca2+ was performed with LC-pro plugin for ImageJ which is available in 
original publication). Software uploaded here is accompanied by input/output data, so users 
can beter understand the structure of folders and files names for correct automated performance 
and fast data analysis as well as a reference of succesful analysis.